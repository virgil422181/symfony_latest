#!/usr/bin/env bash
groupmod -g 1000 www-data
usermod -u 1000 -g 1000 www-data
cd /code && composer install
exec "$@"