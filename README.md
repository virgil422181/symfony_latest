Symfony 6
========================

The "Symfony Demo Application" is a reference application created to show how
to develop Symfony applications following the recommended best practices.

Requirements (docker images)
------------

* PHP 8.0.3 or higher;
* Mysql 8;
* Redis
* Nginx

Installation
------------

Execute this command to build docker images, run containers and install all project dependencies (composer install script ./.docker/php/command-scripts.sh) :

```bash
$ docker compose  -p symfony_latest_test -f ./.docker/docker-compose.yml up --detach
```
Usage
-----

open localhost:80